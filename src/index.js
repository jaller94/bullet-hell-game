import IntroScene from './intro.js';

class BackgroundScene extends Phaser.Scene {
    constructor() {
        super('background');
    }

    preload() {
        this.load.image('background', 'assets/images/background.png');
    }

    create() {
        this.add.image(480 / 2, 640 / 2, 'background');
    }
}

class EnemyBullet extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'bullet2');
    }

    fire(x, y, rotation) {
        this.body.reset(x, y);

        this.setActive(true);
        this.setVisible(true);

        const rad = (rotation) * (Math.PI / 180);
        this.setVelocityX(Math.sin(rad) * 200);
        this.setVelocityY(Math.cos(rad) * 200);
        this.rotation = -rad - Math.PI / 2;
    }

    preUpdate(time, delta) {
        super.preUpdate(time, delta);

        if (this.x <= -32 || this.x >= 512 || this.y <= -32 || this.y >= 632) {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}

class Bullets extends Phaser.Physics.Arcade.Group {
    constructor(scene) {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 2000,
            key: 'bullet2',
            active: false,
            visible: false,
            classType: EnemyBullet,
        });
    }

    fireBullet(x, y, rotation) {
        const bullet = this.getFirstDead(false);
        if (bullet) {
            bullet.fire(x, y, rotation);
        }
    }
}

function pointInRect(x1, y1, x2, y2, w2, h2) {
    return x1 >= x2 && x1 <= x2 + w2 && y1 >= y2 && y1 <= y2 + h2;
}

class Bullet extends Phaser.GameObjects.Image {
    constructor(scene) {
        super(scene, 0, 0, 'bullet');
        this.speed = Phaser.Math.GetSpeed(800, 1);
    }

    fire(x, y) {
        this.setPosition(x, y - 20);
        this.rotation = -90 * (Math.PI/180);

        this.setActive(true);
        this.setVisible(true);
    }

    update(time, delta) {
        this.y -= this.speed * delta;

        if (this.y < -50) {
            this.setActive(false);
            this.setVisible(false);
        }

        for (const enemy of this.scene.enemies.getChildren()) {
            if (!enemy.active) continue;
            if (pointInRect(this.x, this.y, enemy.x, enemy.y, enemy.width, enemy.height)) {
                enemy.health -= 10;
                this.setActive(false);
                this.setVisible(false);
            }
        }
    }
}

class Enemy extends Phaser.GameObjects.Image {
    constructor(scene) {
        super(scene, 0, 0, 'enemy');

        this.speed = Phaser.Math.GetSpeed(400, 1);
        this.bulletGroup = null;
    }
    
    spawn(x, y, bulletGroup, shift = 0) {
        this.setPosition(x, y);
        this.bulletGroup = bulletGroup;
        this.a = 0;
        this.shift = shift;
        this.health = 20;

        this.setActive(true);
        this.setVisible(true);

        this.interval = setInterval(() => {
            this.fire();
        }, 120);
    }

    update(time, delta) {
        if (this.health <= 0) {
            // TODO Explosion
            clearInterval(this.interval);
            this.setActive(false);
            this.setVisible(false);
        }
        this.x = (Math.sin((time / 400) + this.shift) * 150) + 240;
    }

    fire() {
        for (let i = -20; i <= 20; i += 10) {
            if (i === 0) continue;
            this.bulletGroup.fireBullet(this.x, this.y + 8, i + (i < 0 ? this.a : -this.a));
        }
        this.a += 4;
        if (this.a > 20) this.a = -20;
    }
}

export class GameScene extends Phaser.Scene {
    constructor() {
        super('game');
        this.lastFired = 0;
    }

    preload () {
        this.load.image('ship', 'assets/images/player.png');
        this.load.image('bullet', 'assets/images/bullet1.png');
        this.load.image('bullet2', 'assets/images/bullet2.png');
        this.load.image('enemy', 'assets/images/enemy1.png');
        this.load.audio('shot', 'assets/sounds/sfx_wpn_laser6.ogg');
        this.load.audio('theme1', [
            'assets/music/Juhani Junkala [Retro Game Music Pack] Level 1.ogg',
        ]);
    }

    create () {
        this.playerShootsSound = this.sound.add('shot');
        const music = this.sound.add('theme1');
        music.play({
            loop: true,
        });

        // Limited to 20 objects in the pool, not allowed to grow beyond it
        // bullets = this.pool.createObjectPool(Bullet, 20);

        this.bullets = this.add.group({
            classType: Bullet,
            maxSize: 40,
            runChildUpdate: true,
        });

        //  Create the objects in advance, so they're ready and waiting in the pool
        this.bullets.createMultiple({ quantity: 20, active: false });

        // Limited to 20 objects in the pool, not allowed to grow beyond it
        // bullets = this.pool.createObjectPool(Bullet, 20);

        this.enemyBullets = new Bullets(this);

        this.enemies = this.add.group({
            classType: Enemy,
            maxSize: 20,
            runChildUpdate: true,
        });

        //  Create the objects in advance, so they're ready and waiting in the pool
        this.enemies.createMultiple({ quantity: 20, active: false });

        this.ship = this.add.sprite(400, 500, 'ship').setDepth(1);

        this.cursors = this.input.keyboard.createCursorKeys();
        this.space = this.input.keyboard.addKey('SPACE');

        this.speed = Phaser.Math.GetSpeed(300, 1);

        const enemy = this.enemies.get();
        enemy.spawn(0, 50, this.enemyBullets, -1);
        const enemy2 = this.enemies.get();
        enemy2.spawn(0, 100, this.enemyBullets, 1);
        const enemy3 = this.enemies.get();
        enemy3.spawn(0, 150, this.enemyBullets, 3);
    }

    update(time, delta) {
        if (this.cursors.left.isDown) {
            this.ship.x -= this.speed * delta;
        } else if (this.cursors.right.isDown) {
            this.ship.x += this.speed * delta;
        }
        if (this.cursors.up.isDown) {
            this.ship.y -= this.speed * delta;
        } else if (this.cursors.down.isDown) {
            this.ship.y += this.speed * delta;
        }

        if (this.space.isDown && time > this.lastFired) {
            const bullet = this.bullets.get();

            if (bullet) {
                bullet.fire(this.ship.x, this.ship.y);
                this.playerShootsSound.play();

                this.lastFired = time + 50;
            }
        }
    }
}

new Phaser.Game({
    type: Phaser.AUTO,
    width: 480,
    height: 640,
    // backgroundColor: '#2d2d2d',
    parent: 'phaser-example',
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
            gravity: { y: 0 },
            timeScale: 1,
        },
    },
    scene: IntroScene,
    // scene: [
    //     new GameScene(),
    //     new BackgroundScene(),
    // ]
});
