import { GameScene } from './index.js';

class IntoScene extends Phaser.Scene {
    preload = () => {
        // this.load.audio('theme', [
        //     'assets/music/In the circus PSG2.ogg',
        // ]);
        this.load.audio('theme', [
            'assets/music/Juhani Junkala [Retro Game Music Pack] Level 3.ogg',
        ]);
        this.load.image('jet', 'assets/images/f15-rotated.png');
        this.load.video('background', [
            'assets/videos/stock-footage-abstract-red-orange-diagonal-glowing-stripe-background-loop.webm',
        ]);
    }

    create = () => {
        const music = this.sound.add('theme');
        music.play({
            loop: true,
        });

        // Background
        const video = this.add.video(480 / 2, 640 / 2, 'background');
        video.play(true);
        video.displayWidth = 640;
        video.displayHeight = 480;
        video.rotation = -90 * (Math.PI / 180);

        // Jet
        this.jet = this.add.image(480 / 2, 200, 'jet');
        this.jet.rotation = -15 * (Math.PI / 180);

        this.add.text(480 / 2, 480, 'START');
        this.add.text(480 / 2, 520, 'OPTIONS');

        this.input.once('pointerdown', () => {
            this.scene.add('game', GameScene, true);
            music.stop();
            this.scene.remove(this);
        }, this);
    }

    update = (time, delta) => {
        // Jet animation
        this.jet.x = 10 * Math.sin(time / 500) + 480 / 2;
        this.jet.y = 10 * Math.cos(time / 713) + 480 / 2;
        this.jet.rotation = 0.01 * Math.sin(time / 6209) + -22 * (Math.PI / 180);
    }
}

export default IntoScene;

